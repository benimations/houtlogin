<?php
/**
 * @wordpress-plugin
 * Plugin Name:		Houtlogin
 * Description:		Regelt weergave login
 * Version:			1.0.0
 * Author:			Ben
 * Text Domain:		swp
 */
 
 // menubar items
 function swp_hide_all_but_bookly() {
	$swp_bookly_only_user = wp_get_current_user();
	
	if (strpos($swp_bookly_only_user->first_name, "houtloods") !== false) {
		remove_menu_page("link-manager.php");
		remove_menu_page("edit-comments.php");
		remove_menu_page("profile.php");
		remove_menu_page("tools.php");
		remove_menu_page("tools.php");
		
	}
	
 }
add_action( 'admin_menu', 'swp_hide_all_but_bookly', 999 );






// CSS frontend
function swp_bookly_only_register_front_scripts() {
	wp_register_style( 'swp-bookly-only-style', plugins_url( 'css/bookly-only-front.css', __FILE__ ));
	
}
add_action( 'wp_enqueue_scripts', 'swp_bookly_only_register_front_scripts' );

function swp_bookly_only_enqueue_front_scripts() {
	$swp_bookly_only_user = wp_get_current_user();
	
	if (strpos($swp_bookly_only_user->first_name, "houtloods") !== false) {
		wp_enqueue_style('swp-bookly-only-style');
		
	}
	
}
add_action( 'wp_footer', 'swp_bookly_only_enqueue_front_scripts' );






// CSS backend
function swp_bookly_only_register_scripts() {
	wp_register_style( 'swp-bookly-only-style', plugins_url( 'css/bookly-only.css', __FILE__ ));
	
}
add_action( 'admin_enqueue_scripts', 'swp_bookly_only_register_scripts' );

function swp_bookly_only_enqueue_scripts() {
	$swp_bookly_only_user = wp_get_current_user();
	
	if (strpos($swp_bookly_only_user->first_name, "houtloods") !== false) {
		wp_enqueue_style('swp-bookly-only-style');
		
	}
	
}
add_action( 'admin_head', 'swp_bookly_only_enqueue_scripts' );






?>